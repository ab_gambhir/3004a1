package p1;

import p1.client.AppClient;
import p1.utils.Config;

public final class StartClient {

	public static void main (String[] argv) {
		new AppClient(Config.DEFAULT_HOST, Config.DEFAULT_PORT); 
	}
}
