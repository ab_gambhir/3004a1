package p1;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import p1.server.AppServer;
import p1.utils.Config;

@SuppressWarnings("unused")
public class StartServer {
	
	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	private static AppServer appServer = null;
	
	static Logger logger = Logger.getLogger(StartServer.class.getName());
	
	public static void main(String[] argv) {

		//PropertyConfigurator.configure(Config.ServerConfig);
		System.out.println("Starting server ...");

		do {
			System.out.println("startup | shutdown");
			String input = sc.nextLine();
			
			if (input.equalsIgnoreCase("STARTUP") || input.equalsIgnoreCase("START") && !started)
			{
				System.out.println("Starting server ...");
				appServer = new AppServer(Config.DEFAULT_PORT);
				started = Boolean.TRUE;
				logger.info(String.format("Server Started %s:%d", Config.DEFAULT_HOST, Config.DEFAULT_PORT));				
			}
			
			if(input.equalsIgnoreCase("Initialize the game"))
				appServer.initGame();
			
			//hard coding it for 4 players and 6 rounds, but the startGame(int, int) itself is not hard coded
			if(input.equalsIgnoreCase("startGame 4 6"))
				appServer.startGame(4, 6);
			
			if (input.equalsIgnoreCase("SHUTDOWN") && started)
			{
				System.out.println("Shutting server down ...");
				appServer.shutdown();
				started = Boolean.FALSE;
				done = Boolean.TRUE;
				logger.info(String.format("Server Shutdown %s:%d", Config.DEFAULT_HOST, Config.DEFAULT_PORT));			
			}			
		} while (!done);

		System.out.println("Exiting ...");
		System.exit(1);
	}
}
