package p1.Tests;

import static org.junit.Assert.*;
import org.junit.*;
import rulesEngine.RulesEngine;

public class TestRulesEngineOutcome {
	RulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: TestRulesEngineResolution");
	}

	/** This is processed/initialized before each test is conducted */
	@Before
	public void setUp() {
		System.out.println("@Before(): TestRulesEngineResolution");
		rEngine = new RulesEngine();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): TestRulesEngineResolution");
		rEngine = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: TestRulesEngineResolution");
	}

	@Test
	public void testSecondRoll() {
		rEngine.processInput("joe john thrust 3 charge 1");
		rEngine.processRoll(rEngine.roll(), "joe");
		rEngine.processAvD("joe", "john");
		rEngine.processInput("john joe swing 3 charge 1");
		rEngine.processRoll(rEngine.roll(), "john");
		rEngine.processAvD("john", "joe");
		assertTrue(rEngine.players.get("john").length() == rEngine.players.get("john").length());
	}

	@Test
	public void testOutcome() {
		rEngine.processInput("joe john thrust 3 charge 1");
		rEngine.processRoll(rEngine.roll(), "Joe");
		// rEngine.processAvD("joe", "john");
		rEngine.processInput("john joe swing 3 charge 1");
		rEngine.processRoll(rEngine.roll(), "john");
		assertEquals(2, rEngine.players.size());
		// rEngine.processAvD("john", "joe");
		// assertEquals();

	}
}
