package p1.Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import rulesEngine.RulesEngine;

public class TestRulesEngineResolution {
	RulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: TestRulesEngineResolution");
	}

	/** This is processed/initialized before each test is conducted */
	@Before
	public void setUp() {
		System.out.println("@Before(): TestRulesEngineResolution");
		rEngine = new RulesEngine();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): TestRulesEngineResolution");
		rEngine = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: TestRulesEngineResolution");
	}

	@Test
	public void testSpeedTotal() {
		rEngine.processInput("joe john thrust 2 dodge 2");

		assertTrue((Integer.parseInt(rEngine.aSpeed) + Integer.parseInt(rEngine.dSpeed)) > 3);
	}

	@Test
	public void testRollAction() {
		rEngine.processInput("joe john thrust 2 dodge 2");
		if (rEngine.dice == (3 | 4)) {
			assertTrue("smash" == rEngine.action);
		} else if (rEngine.dice == (5 | 6)) {
			assertTrue("swing" == rEngine.action);
		}
	}

	@Test
	public void testActionVDefense() {
		rEngine.processInput("joe john thrust 2 dodge 2");
		rEngine.processInput("john joe swing 3 charge 1");
		rEngine.processAvD("joe", "john");
		String data = rEngine.players.get("john");
		System.out.println(rEngine.players.get("john"));
		String[] datas = data.split(" ", 7);
		assertEquals("1", datas[6]); // wrong, moved on
		// assertTrue(rEngine.players.get("john").split(" ", 7)[6]
		// rEngine.players.get("joe").split(" ", 7)[6]); // );
	}

	@Test
	public void testDoubleWound() {
		rEngine.processInput("joe john thrust 1 dodge 3");
		rEngine.processInput("john gus swing 3 charge 2");
		rEngine.processAvD("joe", "john");
		System.out.println(rEngine);
		assertEquals("2", rEngine.players.get("john").split(" ", 7)[6]);

	}

}
