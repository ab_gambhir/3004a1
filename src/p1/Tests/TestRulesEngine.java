package p1.Tests;

import static org.junit.Assert.assertEquals;

import org.junit.*;

import rulesEngine.RulesEngine;

public class TestRulesEngine {
	RulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: TestRulesEngine");
	}

	/** This is processed/initialized before each test is conducted */
	@Before
	public void setUp() {
		System.out.println("@Before(): TestRulesEngine");
		rEngine = new RulesEngine();
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): TestRulesEngine");
		rEngine = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: TestRulesEngine");
	}

	@Test
	public void testInput() {
		System.out.println("@Test: testInput()");
		// rEngine.setState(RulesEngine.CHOSEN);
		assertEquals("joe john thrust 3 charge 2", rEngine.processInput("joe john smash 3 smash 2"));
		// "originator:joe,terminator:jack,attack:thrust,aSpeed:3,defense:dodge,dSpeed:1"
	}
}
