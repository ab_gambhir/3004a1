package p1.Tests;

import static org.junit.Assert.*;

import org.junit.*;

import p1.server.AppServer;

public class TestServer {
	AppServer server;

	/** This will be processed before the Test Class is instantiated */
	@BeforeClass
	public static void BeforeClass() {
		System.out.println("@BeforeClass: TestServer");
	}

	/** This is processed/initialized before each test is conducted */
	@Before
	public void setUp() {
		System.out.println("@Before(): TestServer");
		server = new AppServer(4500);
	}

	/** This is processed/initialized after each test is conducted */
	@After
	public void tearDown() {
		System.out.println("@After(): TestServer");
		server = null;
	}

	/** This will be processed after the Test Class has been destroyed */
	@AfterClass
	public static void afterClass() {
		System.out.println("@AfterClass: TestServer");
	}

	@Test
	public void testForTwo() {
		server.clientCount = 2;
		assertEquals(2, server.clientCount);
	}

	@Test
	public void testForThree() {
		server.clientCount = 3;
		assertEquals(3,server.clientCount)
	}

	@Test
	public void testForFour() {
		server.clientCount = 4;
		assertTrue(server.clientCount !> 4);
	}
}
