

package p1.server;

import java.net.*;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

//import demo.Player;

import java.io.*;

import p1.utils.Config;
import p1.utils.Formatter;

class Player
{
	public String name, a_Move, d_Move, oName;
	public int a_speed, d_speed, wound = 0;
	
	public Player(Player p)
	{
		this.name = p.name;
		this.a_Move = p.a_Move;
		this.d_Move = p.d_Move;
		this.a_speed = p.a_speed;
		this.d_speed = p.d_speed;
		this.wound = p.wound;
	}
	
	public Player()
	{
		
	}
}

public class AppServer implements Runnable {
	static Logger logger = Logger.getLogger(AppServer.class.getName());
	
	int clientCount = 0;
	private Filter filter;
	private Thread thread = null;
	private ServerSocket server = null;
	private HashMap<Integer, ServerThread> clients;
	private HashMap<Integer, String[]> playersInfo = new HashMap<Integer, String[]>();

	public AppServer(int port) {
		try {
			/** Set up our message filter object */
			filter = new Filter();
			
			System.out.println("Binding to port " + port + ", please wait  ...");
			logger.info("Binding to port " + port);
			/**
			 * I use a HashMap to keep track of the client connections and their
			 * related thread
			 */
			clients = new HashMap<Integer, ServerThread>();

			/** Establish the servers main port that it will listen on */
			server = new ServerSocket(port);
			/**
			 * Allows a ServerSocket to bind to the same address without raising
			 * an "already bind exception"
			 */
			server.setReuseAddress(true);
			start();
		} catch (IOException ioe) {
			logger.fatal(Formatter.exception("Contructor", ioe));
		}
	}

	/** Now we start the servers main thread */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();			
			logger.info(String.format("Server started: %s %d", server,thread.getId()));
		}
	}

	/** The main server thread starts and is listening for clients to connect */
	public void run() {
		while (thread != null) {
			try {
				logger.info("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException ioe) {				
				logger.fatal(Formatter.exception("run()", ioe));
			}}
	}

	/** 
	 * Client connection is accepted and now we need to handle it and register it 
	 * and with the server | HashTable 
	 **/
	private void addThread(Socket socket) {
		logger.info(Formatter.format("Client Requesting connection: " , socket));
		if (clientCount < Config.MAX_CLIENTS) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(this, socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				clients.put(serverThread.getID(), serverThread);
				this.clientCount++;
				logger.info(Formatter.format("Client Accepted: " , socket));
			} catch (IOException ioe) {
				logger.fatal(Formatter.exception("addThread(Socket)", ioe));
			}
		} else {
			logger.info(Formatter.format("Client Tried to connect:", socket));
			logger.info(Formatter.format("Client refused: maximum number of clients reached:", Config.MAX_CLIENTS));
		}
	}

	public synchronized void handle(int ID, String input) {
		if (input.equals("quit!")) 
		{
			logger.info(Formatter.format("Removing Client:", ID));
			if (clients.containsKey(ID)) {
				clients.get(ID).send("quit!" + "\n");
				remove(ID);
			}
		}

		else

		if (input.equals("shutdown!")) { shutdown(); }

		else 
		{
			ServerThread from = clients.get(ID);
			for (ServerThread to : clients.values()) {
				if (to.getID() != ID) {
				
						String[] data = input.split(" ");
						playersInfo.put(ID, data);
						logger.info(Formatter.formatChatMessage(from,to,input));
					}
				}
			}
		}
	}

	/** Try and shutdown the client cleanly */
	public synchronized void remove(int ID) {
		if (clients.containsKey(ID)) {
			ServerThread toTerminate = clients.get(ID);
			clients.remove(ID);
			clientCount--;

			toTerminate.close();
			toTerminate = null;
		}
	}

	/** Shutdown the server cleanly */
	public void shutdown() {
		Set<Integer> keys = clients.keySet();

		if (thread != null) {
			thread = null;
		}

		try {
			for (Integer key : keys) {
				clients.get(key).close();
			}
			clients.clear();
			server.close();
		} catch (IOException ioe) {
			logger.fatal(Formatter.exception("shutdown()", ioe));
		}
		logger.info(String.format("Server Shutdown cleanly %s", server));
	}

	//starting game
	public void initGame()
	{
		if(clientCount >= 2 && clientCount <= 4)
		{
			System.out.println("Number of players is enough, game can begin");
			for (Integer key : clients.keySet()) {
    			System.out.println("Key = " + key);
    			clients.get(key).send("Please enter your details" + "\n");
			}
			
		}
		else 
			System.out.println("Number of players is invalid");
	}

	public void startGame(int numPlayers, int numRounds)
	{
		// System.out.println("Details given by the players: ");
		Player player;
		Player opponent = new Player();
		Player[] players = new Player[numRounds];
		int a = 0, dice = 0;
		String final_attack_move = "";
		Random rand = new Random();
		for(Integer key : playersInfo.keySet())
		{
			String[] data = playersInfo.get(key);
			player = new Player();
			player.name = data[0];
			player.oName = data[1];
			player.a_Move = data[2];
			player.a_speed = Integer.parseInt(data[3]);
			player.d_Move = data[4];
			player.d_speed = Integer.parseInt(data[5]);
			players[a] = player;
			if(a<=numPlayers)
				++a;

		}
		for(int r = 1; r <= numRounds; r++)
		{
			for(int i = 0; i < clientCount; i++)
			{
				//finding the final attack move based on die roll
				dice = rand.nextInt(6) + 1;
				
				if(dice == 1 || dice == 2)
				{
					final_attack_move = players[i].a_Move;
				}
				else if(dice == 3 || dice == 4)
				{
					if(players[i].a_Move.equalsIgnoreCase("thrust"))
						final_attack_move = "smash"; 
					else if(players[i].a_Move.equalsIgnoreCase("smash"))
						final_attack_move = "swing";
					else if(players[i].a_Move.equalsIgnoreCase("swing"))
						final_attack_move = "thrust";
				}
				else if(dice == 5 || dice == 6)
				{
					if(players[i].a_Move.equalsIgnoreCase("thrust"))
						final_attack_move = "swing"; 
					else if(players[i].a_Move.equalsIgnoreCase("smash"))
						final_attack_move = "thrust";
					else if(players[i].a_Move.equalsIgnoreCase("swing"))
						final_attack_move = "smash";
				}
				
				//finding the opponent
				for(int j = 0; j < clientCount; j++)
				{
					if(players[i].oName.equalsIgnoreCase(players[j].name))
						opponent = new Player(players[j]);
				}
				
				//resolving the attack
				if(final_attack_move == "thrust" && opponent.d_Move == "charge")
					opponent.wound +=1;
				else if(final_attack_move == "swing" && opponent.d_Move == "dodge")
					opponent.wound +=1;
				else if(final_attack_move == "smash" && opponent.d_Move == "duck")
					opponent.wound +=1;
				else if(players[i].a_speed < opponent.d_speed)
					opponent.wound +=1;
				
				if(opponent.wound > 0)
					System.out.println("The player " + opponent.name + " was wounded " + opponent.wound + " times by player " + players[i].name);
				else
					System.out.println(players[i].name + " could not wound their opponent " + opponent.name);
				
				System.out.println("--------------------------------------------------");
				
			}

		}
	}
}
